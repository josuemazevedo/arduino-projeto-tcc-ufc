#include <Stepper.h> 
int dados = 0;
const int stepsPerRevolution = 500;
bool controle = false;
int cont = 0;
  
//Inicializa a biblioteca utilizando as portas de 8 a 11 para 
//ligacao ao motor 
Stepper myStepper(stepsPerRevolution, 8,10,9,11); 

void setup() {
  // put your setup code here, to run once:
  
  Serial.begin(9600);
  //Determina a velocidade inicial do motor 
  myStepper.setSpeed(50);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available() > 0) {
    // get incoming byte:
    dados = Serial.read();
    // 0 - Inicia e 1 - Para a rotação
    if(dados=='0'){
      controle = true;
    }
    if(dados=='1'){
      controle = false;    
    }                
   }
   if(controle){
      for(int i =0;i<185;i++){
        myStepper.step(-300);
        
      }
      for(int i =0;i<185;i++){
        myStepper.step(+300);
      }      
   }
}


